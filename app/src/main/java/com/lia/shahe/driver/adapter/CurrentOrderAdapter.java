package com.lia.shahe.driver.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.driver.CorderDetails;
import com.lia.shahe.driver.CurrentOrders;
import com.lia.shahe.driver.ForgotPassword;
import com.lia.shahe.driver.MainActivity;
import com.lia.shahe.driver.R;
import com.lia.shahe.driver.bo.Corder;
import com.lia.shahe.driver.utility.Session;
import com.lia.shahe.driver.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class CurrentOrderAdapter  extends RecyclerView.Adapter<CurrentOrderAdapter.ViewHolder> {
    List<Corder> list_order;
    Context ct;
    private Session session;
    private String m_Text = "";

    private CurrentOrders currentOrders;

    public CurrentOrderAdapter(List<Corder> list_order, Context ct, Session session, CurrentOrders currentOrders) {
        this.list_order = list_order;
        this.ct = ct;
        this.session = session;
        this.currentOrders=currentOrders;

    }

    @Override
    public CurrentOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.corder_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(CurrentOrderAdapter.ViewHolder holder, int position) {
        final Corder listCOrderData = list_order.get(position);

        holder.ordnoval.setText(listCOrderData.getaOid());
        holder.ctot.setText(listCOrderData.getoTotal()+" SAR");
        holder.cpaytypeval.setText(listCOrderData.getpType());

//        holder.statusval.setText(listCOrderData.getoStatus());
        holder.dateval.setText(listCOrderData.getoDate());

        try {
            JSONObject cvalobj=new JSONObject(listCOrderData.getcDetails());
            holder.cnameval.setText(cvalobj.getString("custName"));
            holder.cmobnoval.setText(cvalobj.getString("custPhone"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject avalobj=new JSONObject(listCOrderData.getaDetails());
            holder.caddressval.setText(avalobj.getString("address"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

                //create click listener
        holder.statusval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("asdfghjkl;",""+listCOrderData.getOid());
                currentOrders.statusUpdate(listCOrderData.getOid());

// Set up the buttons




            }
        });

        holder.blinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("asdc",""+listCOrderData.getoDetails());
                Intent intent=new Intent(ct, CorderDetails.class);
                intent.putExtra("details",listCOrderData.getoDetails());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ct.startActivity(intent);
            }
        });
        holder.track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject avalobj= null;
                try {
                    avalobj = new JSONObject(listCOrderData.getaDetails());
                    Log.d("asdc",""+listCOrderData.getoDetails());
                    Uri gmmIntentUri = Uri.parse("google.navigation:q="+avalobj.getDouble("latitude")+","+avalobj.getDouble("longitude"));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ct.startActivity(mapIntent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });



    }

    private void statusAction(int oid) {
//        mainActivity.statusUpdate(oid);
//        mainActivity.test();
    }


    @Override
    public int getItemCount() {
        return list_order.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView ordnoval,dateval,statusval,cnameval,cmobnoval,caddressval,cpaytypeval,ctot,track;
        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);

            ordnoval = (TextView) itemView.findViewById(R.id.ordtxt);
            dateval = (TextView) itemView.findViewById(R.id.odate);
            statusval = (TextView) itemView.findViewById(R.id.ostatus);
            cnameval = (TextView) itemView.findViewById(R.id.customertxt);
            cmobnoval = (TextView) itemView.findViewById(R.id.phonetxt);
            caddressval = (TextView) itemView.findViewById(R.id.addresstxt);
            cpaytypeval = (TextView) itemView.findViewById(R.id.paytypetxt);
            ctot = (TextView) itemView.findViewById(R.id.totaltxt);
            track = (TextView) itemView.findViewById(R.id.tracktxt);
            blinear = (LinearLayout) itemView.findViewById(R.id.viewlinear);
        }
    }
}