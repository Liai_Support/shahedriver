package com.lia.shahe.driver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.driver.utility.Session;
import com.lia.shahe.driver.utility.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener{

    private EditText cpswdtext,pswdtext,conpswdtext;
    private TextView btntext;
    private long lastClickTime = 0;
    private String Url="changepassword";
    private ImageView hfhome,finfo,fcart,flist,fprofile;

    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        session = new Session(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        cpswdtext=(EditText)findViewById(R.id.cpswdtxt);
        pswdtext=(EditText)findViewById(R.id.pswdtxt);
        conpswdtext=(EditText)findViewById(R.id.conpswdtxt);

        btntext=(TextView) findViewById(R.id.btntxt);

        btntext.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();

        if (view==btntext){
            String cpswdStr=cpswdtext.getText().toString();
            String pswdStr=pswdtext.getText().toString();
            String conpswdStr=conpswdtext.getText().toString();
            Log.d("asd1",cpswdStr);
            Log.d("asd2",pswdStr);
            Log.d("asd3",conpswdStr);
            if (pswdStr=="" || cpswdStr=="" || conpswdStr==""){
                Toast.makeText(getApplicationContext(),getString(R.string.enterallfield),Toast.LENGTH_SHORT).show();

            }else if(!pswdStr.equals(conpswdStr)){
                Toast.makeText(getApplicationContext(),getString(R.string.paswordnotmatch),Toast.LENGTH_SHORT).show();
            }else{
                JSONObject requstbody = new JSONObject();
                try {
                    requstbody.put("userid",String.valueOf(session.getUserId()));
                    requstbody.put("password",pswdStr);
                    requstbody.put("oldPassword",cpswdStr);

                    requestJSON(requstbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }



    }

    private void requestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);


                    }
//                        obj.getInt()

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);


    }
    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(ChangePassword.this, MainActivity.class);
        startActivity(intent);
    }


}