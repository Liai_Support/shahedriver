package com.lia.shahe.driver;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.driver.adapter.CurrentOrderAdapter;
import com.lia.shahe.driver.bo.Corder;
import com.lia.shahe.driver.utility.Session;
import com.lia.shahe.driver.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CurrentOrders extends Fragment {
    private static final String Url = "currentorder";
    private static final String statusUrl = "updatestatus";
    int dummyColor;
    private List<Corder> list_order;
    private Session session;
    private  MainActivity mainActivity;

    private CurrentOrderAdapter corderAdapter;
    private GridLayoutManager brgm;
    private RecyclerView corderrv;

    public CurrentOrders() {
    }

    @SuppressLint("ValidFragment")
    public CurrentOrders(int color) {
        this.dummyColor = color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_current_orders, container, false);



        corderrv=view.findViewById(R.id.corder_rc);
        brgm=new GridLayoutManager(getActivity().getBaseContext(),1,GridLayoutManager.VERTICAL, false);

        corderrv.setLayoutManager(brgm);

        JSONObject obj=new JSONObject();
        try {
            obj.put("driverId", StaticInfo.userId);
            getInitialValues(String.valueOf(obj));

            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    private void stopLoader() {
        LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        loadingDialog.dismissDialog();
    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);


            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        loadingDialog.dismissDialog();
                            Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }else{
//                            JSONObject dataobj=obj.getJSONObject("data");

                            list_order=new ArrayList<>();
                            JSONArray branchArray=obj.getJSONArray("data");
                            for (int i = 0; i < branchArray.length(); i++) {
                                JSONObject corderobj = branchArray.getJSONObject(i);
                                Corder corder = new Corder(corderobj.getInt("orderid"),corderobj.getString("applicationOrderId"),
                                        corderobj.getString("orderTotal"),corderobj.getString("paymentType"),corderobj.getString("orderDate"),corderobj.getString("orderStatus"),
                                        corderobj.getString("orderdetails"),corderobj.getString("customerdetails"),corderobj.getString("addressdetails"));
                                list_order.add(corder);

                            }
                            setupCOrderData(list_order);

//                            StaticInfo.cartCount= Integer.parseInt(dataobj.getString("cartcount"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupCOrderData(List<Corder> list_order) {
        corderAdapter=new CurrentOrderAdapter(list_order,getActivity().getBaseContext(),session,this);
        corderrv.setAdapter(corderAdapter);
    }

    public void statusUpdate(final int oid) {
        Log.d("asdfghj",""+oid);



        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.alert));
        builder.setMessage("Are you sure to update the delivery status");


// Set up the input
        final EditText input = new EditText(getContext());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        input.setHint("جاهز للتوصيل");
        builder.setView(input);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                String emailStr= String.valueOf(m_Text);
                JSONObject requstbody = new JSONObject();
                try {
                    requstbody.put("orderid",""+oid);
                    requstbody.put("status","Delivered");
                    requstbody.put("deliverynote",m_Text);

                    statusrequestJSON(requstbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void statusrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
//            String URL = "http://...";
//            JSONObject jsonBody = new JSONObject(response);
        final String requestBody = response.toString();
        Log.d("requestjsondfdgf", String.valueOf(response));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.MAIN_URL+statusUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("strrrrsdfr", ">>" + response);
//
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error")==true){
                        String msg=obj.getString("message");
                        Toast.makeText(getActivity().getBaseContext(),msg,Toast.LENGTH_SHORT).show();
                        Log.d("signin response", ">>" + "error");
                    }else if (obj.getBoolean("error")==false){
                        String msg=obj.getString("message");
                        Toast.makeText(getActivity().getBaseContext(),msg,Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(getContext(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                      startActivity(i);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);


    }
}