package com.lia.shahe.driver.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context _context;
    private static final String IS_LOGIN = "IsLoggedIn";

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences constants
    private static final String PREF_NAME = "MyPreference";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";


    public Session(Context context) {
        this._context = context;
        prefs = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = prefs.edit();
    }
    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return prefs.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }



    public void setusename(String usename) {
        prefs.edit().putString("usename", usename).commit();
    }

    public String getarticle() {
        String article = prefs.getString("article","");
        return article;
    }
    public void setarticle(String article) {
        prefs.edit().putString("article", article).commit();
    }

    public String getusename() {
        String usename = prefs.getString("usename","");
        return usename;
    }
    public void setUserId(int userId) {
        prefs.edit().putInt("userId", userId).commit();
    }

    public int getUserId() {
        int userId = prefs.getInt("userId",0);
        return userId;
    }
    public void setBranchId(int BranchId) {
        prefs.edit().putInt("BranchId", BranchId).commit();
    }

    public int getBranchId() {
        int BranchId = prefs.getInt("BranchId",0);
        return BranchId;
    }
    public void setBranchAddr(String Addrs) {
        prefs.edit().putString("Addrs",Addrs ).apply();
    }

    public String getBranchAddr() {
        String Addrs = prefs.getString("Addrs","");
        return Addrs;
    }
    public void setlang(String Lang) {
        prefs.edit().putString("Lang",Lang).commit();
    }

    public String getlang() {
        String Lang = prefs.getString("Lang","ta");
        return Lang;
    }


    public void setacontent(String acontent) {
        prefs.edit().putString("acontent",acontent).apply();
    }

    public String getacontent() {
        String acontent = prefs.getString("acontent","");
        return acontent;
    }
    public void setavat(String avat) {
        prefs.edit().putString("avat",avat).apply();
    }

    public String getavat() {
        String avat = prefs.getString("avat","");
        return avat;
    }
    public void setahelp(String ahelp) {
        prefs.edit().putString("ahelp",ahelp).apply();
    }

    public String getahelp() {
        String ahelp = prefs.getString("ahelp","");
        return ahelp;
    }
    public void setacontactUs(String acontactUs) {
        prefs.edit().putString("acontactUs",acontactUs).apply();
    }

    public String getcontactUs() {
        String acontactUs = prefs.getString("acontactUs","");
        return acontactUs;
    }
  }
