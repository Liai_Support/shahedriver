package com.lia.shahe.driver.utility;


public class StaticInfo {

   //public static String MAIN_URL="http://192.168.0.104/uaservice/tech.php?action=";
   //  public static String MAIN_URL="http://env-6004661.cloud.cms500.com/uaservice/tech.php?action=";

   //public static String MAIN_URL="http://universalairconditioner.com/uaservice/tech.php?action=";


    public static String MAIN_URL="https://www.yellostack.com/shaheapi/driver/";

//    public static String RATE_URL="http://192.168.1.31/uaservice/rate.txt";

    public static String otpvalue="";

    public static boolean credit=false;



    public static int userId=0;
    public static int selProdId=0;
    public static int cartCount=0;
    public static int vatPrice=0;
    public static String mobNo="";
    public static Double cartPrice= Double.valueOf(0);
    public static Double cartTotal= Double.valueOf(0);





    public static boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }



    public static String getLoginUrl(String mobile, String password){

        String finalUrl=MAIN_URL+"driverlogin";
        finalUrl+="&mobile="+mobile;
        finalUrl+="&password="+password;

        return finalUrl;
    }




    public static boolean validEmail(String email) {
        // editing to make requirements listed
        // return email.matches("[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}");
        return email.matches("[A-Z0-9._%+-][A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{3}");
    }

    public static boolean mobile(String val){
        try{
            Long.parseLong(val);
            if(val.length()==10){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }


}
