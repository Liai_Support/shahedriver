package com.lia.shahe.driver.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lia.shahe.driver.PorderDetails;
import com.lia.shahe.driver.R;
import com.lia.shahe.driver.bo.Porder;
import com.lia.shahe.driver.utility.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PastOrderAdapter  extends RecyclerView.Adapter<PastOrderAdapter.ViewHolder> {
    List<Porder> list_order;
    Context ct;
    private Session session;

    public PastOrderAdapter(List<Porder> list_order, Context ct, Session session) {
        this.list_order = list_order;
        this.ct = ct;
        this.session = session;
    }

    @Override
    public PastOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.porder_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PastOrderAdapter.ViewHolder holder, int position) {
        final Porder listCOrderData = list_order.get(position);

        holder.ordnoval.setText(listCOrderData.getaOid());
        holder.ctot.setText(listCOrderData.getoTotal()+" SAR");
        holder.cpaytypeval.setText(listCOrderData.getpType());

        holder.statusval.setText(listCOrderData.getoStatus());
        holder.dateval.setText(listCOrderData.getoDate());
        try {
            JSONObject cvalobj=new JSONObject(listCOrderData.getcDetails());
            holder.cnameval.setText(cvalobj.getString("custName"));
            holder.cmobnoval.setText(cvalobj.getString("custPhone"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject avalobj=new JSONObject(listCOrderData.getaDetails());
            holder.caddressval.setText(avalobj.getString("address"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //create click listener
        holder.blinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("asdc",""+listCOrderData.getoDetails());
                Intent intent=new Intent(ct, PorderDetails.class);
                intent.putExtra("details",listCOrderData.getoDetails());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ct.startActivity(intent);
            }
        });


//        holder.blinear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                session.setBranchId(listBranchData.getId());
//                session.setBranchAddr(listBranchData.getAddress());
//                Intent intent = new Intent(ct, HomeScreen.class);
//                ct.startActivity(intent);
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return list_order.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView ordnoval,dateval,statusval,cnameval,cmobnoval,caddressval,cpaytypeval,ctot;
        private LinearLayout blinear;

        public ViewHolder(View itemView) {
            super(itemView);

            ordnoval = (TextView) itemView.findViewById(R.id.ordtxt);
            dateval = (TextView) itemView.findViewById(R.id.odate);
            statusval = (TextView) itemView.findViewById(R.id.ostatus);
            cnameval = (TextView) itemView.findViewById(R.id.customertxt);
            cmobnoval = (TextView) itemView.findViewById(R.id.phonetxt);
            caddressval = (TextView) itemView.findViewById(R.id.addresstxt);
            cpaytypeval = (TextView) itemView.findViewById(R.id.paytypetxt);
            ctot = (TextView) itemView.findViewById(R.id.totaltxt);
            blinear = (LinearLayout) itemView.findViewById(R.id.viewlinear);
        }
    }
}