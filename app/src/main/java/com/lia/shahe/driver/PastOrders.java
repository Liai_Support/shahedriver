package com.lia.shahe.driver;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.shahe.driver.adapter.PastOrderAdapter;
import com.lia.shahe.driver.bo.Porder;
import com.lia.shahe.driver.utility.Session;
import com.lia.shahe.driver.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class PastOrders extends Fragment {
    private static final String Url = "pastorders";
    int dummyColor;
    private List<Porder> list_order;
    private Session session;

    private PastOrderAdapter corderAdapter;
    private GridLayoutManager brgm;
    private RecyclerView corderrv;

    public PastOrders() {
    }

    @SuppressLint("ValidFragment")
    public PastOrders(int color) {
        this.dummyColor = color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_past_orders, container, false);



        corderrv=view.findViewById(R.id.corder_rc);
        brgm=new GridLayoutManager(getActivity().getBaseContext(),1,GridLayoutManager.VERTICAL, false);

        corderrv.setLayoutManager(brgm);

        JSONObject obj=new JSONObject();
        try {
            obj.put("driverId", StaticInfo.userId);
            getInitialValues(String.valueOf(obj));

            Log.d("initializeuserId",""+obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void stopLoader() {
        LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        loadingDialog.dismissDialog();
    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);


            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.MAIN_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }else{
//                            JSONObject dataobj=obj.getJSONObject("data");

                            list_order=new ArrayList<>();
                            JSONArray branchArray=obj.getJSONArray("data");
                            for (int i = 0; i < branchArray.length(); i++) {
                                JSONObject corderobj = branchArray.getJSONObject(i);
                                Porder corder = new Porder(corderobj.getInt("orderid"),corderobj.getString("applicationOrderId"),
                                        corderobj.getString("orderTotal"),corderobj.getString("paymentType"),corderobj.getString("orderDate"),corderobj.getString("orderStatus"),
                                        corderobj.getString("orderdetails"),corderobj.getString("customerdetails"),corderobj.getString("addressdetails"));
                                list_order.add(corder);

                            }
                            setupCOrderData(list_order);

//                            StaticInfo.cartCount= Integer.parseInt(dataobj.getString("cartcount"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupCOrderData(List<Porder> list_order) {
        corderAdapter=new PastOrderAdapter(list_order,getActivity().getBaseContext(),session);
        corderrv.setAdapter(corderAdapter);
    }
}