package com.lia.shahe.driver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.lia.shahe.driver.adapter.PorderDetailsAdapter;
import com.lia.shahe.driver.bo.PorderDetailsBo;
import com.lia.shahe.driver.utility.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PorderDetails extends AppCompatActivity {
    private  String oDetails="";
    private List<PorderDetailsBo> list_orderbo;
    private ArrayList<PorderDetailsBo> arraylist;
    private Session session;
    private PorderDetailsAdapter orderdetadapter;
    private GridLayoutManager crgm;
    private RecyclerView cartrv;
    private ImageView hfhome,finfo,fcart,flist,fprofile;
    private long lastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corder_details);
        oDetails=getIntent().getStringExtra("details");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        session = new Session(this);
        this.list_orderbo = list_orderbo;

        cartrv=(RecyclerView)findViewById(R.id.crecycler_view);
        crgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        cartrv.setLayoutManager(crgm);
        Log.d("detailsact",oDetails);

        list_orderbo=new ArrayList<>();

        try {
            JSONArray dataArray = new JSONArray(oDetails);
            Log.d("detailsactasd",""+dataArray);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject orderobj = dataArray.getJSONObject(i);
                PorderDetailsBo orderdet = new PorderDetailsBo(orderobj.getString("details"));
                list_orderbo.add(orderdet);
                setuporderData(list_orderbo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setuporderData(List<PorderDetailsBo> list_orderbo) {
        orderdetadapter=new PorderDetailsAdapter(list_orderbo,this,session,this);

        cartrv.setAdapter(orderdetadapter);
    }



    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(PorderDetails.this, MainActivity.class);
        startActivity(intent);
    }

}