package com.lia.shahe.driver.bo;

public class Porder {

    private int oid;
    private String aOid;
    private String oTotal;
    private String pType;
    private String oDate;
    private String oStatus;
    private String oDetails;
    private String cDetails;
    private String aDetails;

    public Porder(int oid ,String aOid,String oTotal,String pType,String oDate,String oStatus,String oDetails,String cDetails,String aDetails) {
        this.oid = oid;
        this.aOid = aOid;
        this.oTotal = oTotal;
        this.pType = pType;
        this.oDate = oDate;
        this.oStatus = oStatus;
        this.oDetails = oDetails;
        this.cDetails = cDetails;
        this.aDetails = aDetails;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getaOid() {
        return aOid;
    }

    public void setaOid(String aOid) {
        this.aOid = aOid;
    }

    public String getoTotal() {
        return oTotal;
    }

    public void setoTotal(String oTotal) {
        this.oTotal = oTotal;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getoDate() {
        return oDate;
    }

    public void setoDate(String oDate) {
        this.oDate = oDate;
    }

    public String getoStatus() {
        return oStatus;
    }

    public void setoStatus(String oStatus) {
        this.oStatus = oStatus;
    }

    public String getoDetails() {
        return oDetails;
    }

    public void setoDetails(String oDetails) {
        this.oDetails = oDetails;
    }

    public String getcDetails() {
        return cDetails;
    }

    public void setcDetails(String cDetails) {
        this.cDetails = cDetails;
    }

    public String getaDetails() {
        return aDetails;
    }

    public void setaDetails(String aDetails) {
        this.aDetails = aDetails;
    }
}
